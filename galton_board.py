########## Author: Toral Gupta #################
######### Galton Board Problem #################

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

n_balls =  20000
n_rows = 10
n_columns = n_rows +1

board = np.zeros(n_columns)
for i in range (0,n_balls):
	val = 0
	for j in range (0,n_rows):
		z = np.random.choice([0,1])
		val = val + z

	board[val] = board[val] + 1

# Plotting board
plt.plot(board,label='Galton')

# Creating a Gaussian
n_gauss = 100
gauss_val = np.zeros(n_gauss+1)
std = np.sqrt(n_columns*0.5*0.5)
mean = n_balls*0.5
a = np.max(board)
b = n_rows/2
c = std
x = np.linspace(0,n_rows,n_gauss+1)
for i in range (0,n_gauss+1):
	gauss_val[i] = a*np.exp(-(x[i]-b)**2/(2*c**2))
# plt.plot(n_columns, 1/(std * np.sqrt(2 * np.pi)) * np.exp( - (n_columns - mean**2 / (2 * std**2) )),linewidth=2, color='r')
plt.plot(x,gauss_val,label='Gaussian')

plt.xlabel('Slot Number')
plt.ylabel('Number of balls')
plt.legend()
plt.show()

